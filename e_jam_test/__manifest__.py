# -*- coding: utf-8 -*-
#################################################################################
# Author      : Devang H. Pipaliya
# Copyright(c): 2020-Present Devang H. Pipaliya.
# All Rights Reserved.
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#################################################################################
{
    'name': 'E-Jam Test',
    'version': '13.0.1.0.0',
    'author': 'Devang Pipaliya',
    'website': "",
    'depends': ['base', 'product'],
    'data': [],
    'qweb': [],
    'installable': True,
    'auto_install': False,
}

# -*- coding: utf-8 -*-
#################################################################################
# Author      : Devang H. Pipaliya
# Copyright(c): 2020-Present Devang H. Pipaliya.
# All Rights Reserved.
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#################################################################################

from odoo import http, _
from odoo.http import request


class Subscription(http.Controller):

    @http.route(['/get-product/',
        '/get-product/<int:product_id>/'], type='json', auth="public")
    def get_specific_prod_data(self, product_id=None):
        """
        This controller helps to find product info for given product ID
        """
        if not product_id:
            return {
                'error': (_("Missing Product Id"))
            }
        product = request.env['product.product'].sudo().search([('id', '=', product_id)])
        if not product:
            return {
                'error': (_("Product Id not found. Please ensure correct value is been provided."))
            }
        return {
            'name': product.name,
            'sale_price': product.list_price,
            'cost': product.standard_price,
            'on_hand_qty': product.qty_available,
            'category': product.categ_id and product.categ_id.name or "no category assigned",
        }

This Module helps to find the product infor from given product id. If it failes toget the product info, it will return the error messages accordingly.

make sure module installation don't have any error.

How to run api in postman:

URL :
- http://LOCaLHOST:ODOO_PORT/get-product/PRODUCT_ID
- ex. http://0.0.0.0:13056/get-product/20

Method : 
- GET

HEADERS : 
- Content-Type : application/json

Output :
```json
{
    "jsonrpc": "2.0",
    "id": null,
    "result": {
        "name": "Pedal Bin",
        "sale_price": 47.0,
        "cost": 10.0,
        "on_hand_qty": 22.0,
        "category": "Office Furniture"
    }
}
```
